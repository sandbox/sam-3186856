<?php

namespace Drupal\html_mail;

use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Render\Markup;

/**
 * Replace cores 'php_mail' plugin with an HTML equipped version.
 */
class HtmlPhpMail extends PhpMail {

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    // Skip filtering HTML tags if the email has a Content-Type header
    // indicating it should be an HTML email.
    $html_enabled = isset($message['headers']['Content-Type']) && strpos($message['headers']['Content-Type'], 'text/html') !== FALSE;
    if (!$html_enabled) {
      return parent::format($message);
    }

    // Merge all lines in the e-mail body and treat the result as safe markup.
    $message['body'] = Markup::create(implode("\n\n", $message['body']));

    $message_wrapped = [
      '#theme' => 'html_mail_wrapper',
      '#body' => $message['body'],
      '#message' => $message,
    ];
    $message['body'] = \Drupal::service('renderer')->renderPlain($message_wrapped);

    return $message;
  }

}
