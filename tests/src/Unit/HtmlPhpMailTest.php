<?php

namespace Drupal\Tests\html_mail\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Render\RendererInterface;
use Drupal\html_mail\HtmlPhpMail;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * Test the HTML mail class.
 *
 * @group html_mail
 * @coversDefaultClass \Drupal\html_mail\HtmlPhpMail
 */
class HtmlPhpMailTest extends UnitTestCase {

  /**
   * The HTML mail class.
   *
   * @var \Drupal\html_mail\HtmlPhpMail
   */
  protected $mail;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $container = new ContainerBuilder();
    $config_factory = $this->prophesize(ConfigFactoryInterface::class);

    $renderer = $this->prophesize(RendererInterface::class);
    $renderer->renderPlain(Argument::any())->will(function ($args) {
      return $args[0];
    });

    $container->set('config.factory', $config_factory->reveal());
    $container->set('renderer', $renderer->reveal());
    \Drupal::setContainer($container);

    $this->mail = new HtmlPhpMail();
  }

  /**
   * @covers ::format
   */
  public function testFormatHtml() {
    $this->assertFormattedBody(
      'text/html; charset=UTF-8;',
      [
        '<strong>foo</strong>',
        '<em>bar</em>',
      ],
      [
        '#theme' => 'html_mail_wrapper',
        '#body' => "<strong>foo</strong>\n\n<em>bar</em>",
        '#message' => [
          'body' => "<strong>foo</strong>\n\n<em>bar</em>",
          'headers' => ['Content-Type' => 'text/html; charset=UTF-8;'],
        ],
      ],
      );
  }

  /**
   * @covers ::format
   */
  public function testFormatPlain() {
    $this->assertFormattedBody('text/plain', [
      '<strong>foo</strong>',
      '<em>bar</em>',
    ], "*foo*\n\n/bar/\n");
  }

  /**
   * @covers ::format
   */
  public function testFormatNoContentType() {
    $this->assertFormattedBody(NULL, [
      '<strong>foo</strong>',
      '<em>bar</em>',
    ], "*foo*\n\n/bar/\n");
  }

  /**
   * Assert the formatted body of an email using the test mail class.
   */
  protected function assertFormattedBody(?string $contentType, array $body, $expectedFormatting) {
    $formatted = $message = $this->mail->format([
      'body' => $body,
      'headers' => $contentType ? [
        'Content-Type' => $contentType,
      ] : [],
    ]);
    $this->assertEquals($expectedFormatting, $formatted['body']);
  }

}
